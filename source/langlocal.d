/* This file is part of vts-fs-open and it is a modified copy of
 * the module 'langlocal' of the Amalthea library (version 0.5.1).
 * Homepage of Amalthea: https://gitlab.com/tech.vindex/amalthea
 *
 * Copyright (C) 2018-2019 Eugene 'Vindex' Stulin
 *
 * vts-fs-open and the Amalthea library are free software:
 * you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This libary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module langlocal;

import std.array, std.process;

static string[][] localeStrings;
static string[string] langconformity;
static string currentLanguage = "en_US";


/*******************************************************************************
 * Current system language.
 */
string getSystemLanguage() {
    return environment["LANG"].split(".")[0];
}


/*******************************************************************************
 * Current application language by langlocal settings.
 */
string getCurrentLanguage() {
    return currentLanguage;
}


/*******************************************************************************
 * Initializes localization by a two-dimensional array of strings.
 * Titles of columns (first line - index 0) are locales
 * in Linux style without encoding (en_US, en_GB, eo, fr_CA, ru_RU, etc.),
 * first locale is en_US always
 *
 * Params:
 *      stringsWithLocalizations = two-dimensional array containing columns
 *                                 of strings in different languages,
 *      language = new current language (system language by default).
 *
 */
void initLocalization(in string[][] stringsWithLocalizations,
                      string language = "") {
    localeStrings = cast(string[][])stringsWithLocalizations;
    if (localeStrings[0][0] != "en_US") {
        throw new LocalizationsTableException(
            "Error: array in cell [0][0] must consist en_US");
    }
    if (language == "") language = getSystemLanguage;
    chooseLanguage(language);
}


/*******************************************************************************
 * This function allows to choose current locale for application
 */
void chooseLanguage(string language) {
    if (localeStrings.empty) return;
    foreach(i, locale; localeStrings[0]) {
        if (language == locale) {
            currentLanguage = language;
            size_t currentTableColumn = i;
            foreach(row; localeStrings) {
                if (row[currentTableColumn] == "") {
                    langconformity[row[0]] = row[0];
                    } else {
                        langconformity[row[0]] = row[currentTableColumn];
                    }
            }
            return;
        }
    }
    currentLanguage = "en_US";
}


/*******************************************************************************
 * This function returns string corresponding to the English (en_US) version,
 * according to the current localization selected.
 */
string s_(string englishString) {
    return langconformity.get(englishString, englishString);
}
alias _s = s_;


unittest {
    initLocalization([
        ["en_US",    "ru_RU",     "eo"],
        ["Hello",    "Привет",    "Saluton"],
        ["Orange",   "Апельсин",  "Oranĝo"],
        ["Language", "Язык",      "Lingvo"],
        ["Computer", "Компьютер", "Computilo"]
    ]);

    chooseLanguage("ru_RU");
    assert(s_("Orange") == "Апельсин");
    assert("Untranslated string"._s == "Untranslated string");
    assert("Other text" == "Other text".s_);

    chooseLanguage("eo");
    assert("Language"._s == "Lingvo");
    chooseLanguage("Quenya"); //invalid
    assert(getCurrentLanguage == "en_US");
}


class BadLocalException : Exception {
    this(string msg) {
        super(msg);
    }
}


class LocalizationsTableException : Exception {
    this(string msg) {
        super(msg);
    }
}
