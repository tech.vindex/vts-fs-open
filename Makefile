BS=build-scripts
PREFIX=/usr/local
DESTDIR=

DEFAULT_PLANG=D
PLANG=$(DEFAULT_PLANG)
MULTILINGUAL=FALSE
ifeq ($(MULTILINGUAL),TRUE)
	PLANG_DIRNAME=lang_$(PLANG)/
else
	PLANG_DIRNAME=
endif

ifeq ($(PLANG),D)
	DC=ldc2                 # dmd, gdc or ldc2
	PHOBOS_LINKING=dynamic  # static or dynamic
	RELEASE_ARGS=release $(DC) $(PHOBOS_LINKING)
	DEBUG_ARGS=debug $(DC) $(PHOBOS_LINKING)
endif

bin:
	$(BS)/$(PLANG_DIRNAME)compile.sh $(RELEASE_ARGS)

debug:
	$(BS)/$(PLANG_DIRNAME)compile.sh $(DEBUG_ARGS)

test:
	$(BS)/run_tests.sh

update_manpages:
	$(BS)/update_manpages.sh

install:
	$(BS)/install.sh --install $(DESTDIR)$(PREFIX)

uninstall:
	$(BS)/install.sh --uninstall $(DESTDIR)$(PREFIX)

tarball: clean
	$(BS)/build_tarball.sh

clean:
	rm -rf build/

