This project is deprecated. Use: [https://gitlab.com/tech.vindex/ufo](https://gitlab.com/tech.vindex/ufo)

---

vts-fs-open is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

---

## vts-fs-open

vts-fs-open is an alternative instrument for opening files from command line.
The utility opens files using the default applications.

---

## Ready-made packages

See [download page](https://gitlab.com/tech.vindex/vts-fs-open/wikis/Download-page).

---

## Build from source

### Preparing

Before assembling, you need to install a compiler for D (this project supports compilers [dmd](https://dlang.org/download.html#dmd), [gdc](https://gdcproject.org/) and [ldc](https://github.com/ldc-developers/ldc/)) and [chrpath](https://directory.fsf.org/wiki/Chrpath).

For example, in Debian-based distributions, you can install required packages as follows:

`sudo apt install ldc chrpath`

Similarly, in Fedora:

`sudo dnf install ldc chrpath`


### Compilation and installation

Creating of executable bin-file:

`make`

Also, you can choose a compiler for assembling:

`make DC=gdc`

Installation (by default, main directory is /usr/local/):

`sudo make install`

After that, the application is ready for use.

You can install this application in any other directory:

`make install PREFIX=/home/$USER/sandbox`

Uninstall:

`sudo make uninstall`

If you installed in an alternate directory:

`make uninstall PREFIX=/home/$USER/sandbox`

---

## Usage

`vts-fs-open <file list>`

Example:

`vts-fs-open Music/*.mp3`

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
