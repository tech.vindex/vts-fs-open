#!/bin/bash -e
source build-scripts/env.sh
source build-scripts/compilation_settings

SRCDIR=source
SRC="${SRCDIR}/main.d ${SRCDIR}/de.d ${SRCDIR}/langlocal.d"

COMPILATION_MODE=$1 # release or debug
DC=$2               # dmd, ldc2 or gdc
PHOBOS_LINKING=$3   # dynamic or static

# options by compilation settings
LINKING_OPTS=$(eval echo \$${DC^^}_${COMPILATION_MODE^^}_OPTS)
MODE_OPTS=$(eval echo \$${DC^^}_${PHOBOS_LINKING^^}_OPTS)
OUT=$(eval echo \$${DC^^}_OUT)

set -x

mkdir -p ${BINPATH_BASEDIR}

# compile
${DC} ${SRC} ${OUT}${BINPATH} -Jsource/ ${LINKING_OPTS} ${MODE_OPTS}
if [[ $COMPILATION_MODE == release ]]; then
    objcopy --strip-debug --strip-unneeded ${BINPATH} ${BINPATH}
fi

# delete current rpath
pushd "${BINPATH_BASEDIR}"
    chrpath -d ${APP}
    chmod 755 ${APP}
popd

set +x
