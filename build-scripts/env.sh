#!/bin/bash
PROJECT=vts-fs-open
APP=$PROJECT

ALL="changelog COPYING copyright Makefile README.md summary \
build-scripts/ help/ source/ testing/"

readonly VERSION=`cat source/version`

# build path for a executable file
readonly BINPATH_BASEDIR=build/bin
BINPATH="${BINPATH_BASEDIR}/${APP}"

